.PHONY: help

# User Id
UID = $(shell id -u)

## Display this help text
help:
	$(info ---------------------------------------------------------------------)
	$(info -                        Available targets                          -)
	$(info ---------------------------------------------------------------------)
	@awk '/^[a-zA-Z\-\_0-9]+:/ {                                   \
	nb = sub( /^## /, "", helpMsg );                             \
	if(nb == 0) {                                                \
		helpMsg = $$0;                                             \
		nb = sub( /^[^:]*:.* ## /, "", helpMsg );                  \
	}                                                            \
	if (nb)                                                      \
		printf "\033[1;31m%-" width "s\033[0m %s\n", $$1, helpMsg; \
	}                                                              \
	{ helpMsg = $$0 }'                                             \
	$(MAKEFILE_LIST) | column -ts:

.env:
	@echo "You should read and follow instructions from README.md first"
	exit 1

docker-compose.yml:
	@echo "You should select one method, published port or not, see README.md about docker-compose.yml"
	exit 2

config: .env docker-compose.yml

## Pull images used in docker-compose config
pull: config
	docker-compose pull

## Build images from docker-compose config
build: config
	docker-compose build --pull

## Start all the containers
up: config
	docker-compose up -d
## Alias -> up
start: up

## Stop all the containers
stop: config
	docker-compose stop

## Stop, then... start
restart: stop start

## Down all the containers
down: config
	docker-compose down --remove-volumes --orphans

## Enter a shell
shell: config
	docker-compose exec autossh sh

## Show tunnel status
ps: config
	@docker-compose ps
	@echo
	@docker-compose exec autossh netstat -tp

## Show logs
logs: config
	docker-compose logs -tf --tail=1000

## Upgrade the stack
upgrade: pull up

## Local upgrade (build here, without pulling distributed image)
upgrade-local: build up
