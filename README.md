# autossh - docker ssh tunnels

A quick way to open ssh tunnels

## Overview

![schema](doc/tunnel.png)

## Setup

```shell
git clone git@gitlab.com:kilik/docker/autossh.git
cd autossh
```

### To open a port into a docker network only

```shell
cp .env.dist .env
# fix some values in .dev
ln -s docker-compose.yml.dist docker-compose.yml
make upgrade
```

### To open a port on docker host

```shell
cp .env.port.dist .env
# fix some values in .dev
ln -s docker-compose.port.yml.dist docker-compose.yml
make upgrade
```

## To work on this project (without using pre-build images)

```shell
make upgrade-local
```
