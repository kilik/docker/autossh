#!/bin/sh

touch ${SSH_KEY_FILE:=/id_rsa}
chmod 0400 ${SSH_KEY_FILE:=/id_rsa}

STRICT_HOSTS_KEY_CHECKING=no
KNOWN_HOSTS=${SSH_KNOWN_HOSTS:=/known_hosts}
if [ -f "${KNOWN_HOSTS}" ]; then
    chmod 0400 ${KNOWN_HOSTS}
    KNOWN_HOSTS_ARG="-o UserKnownHostsFile=${KNOWN_HOSTS}"
    STRICT_HOSTS_KEY_CHECKING=yes
fi

echo "[INFO] Tunneling ${SSH_USER}@${SSH_HOSTNAME} (${REMOTE_HOST}:${REMOTE_PORT}) to ${LOCAL_PORT}"

AUTOSSH_PIDFILE=/autossh.pid \
AUTOSSH_POLL=10 \
AUTOSSH_LOGLEVEL=0 \
AUTOSSH_LOGFILE=/dev/stdout \
autossh \
 -M 0 \
 -o StrictHostKeyChecking=${STRICT_HOSTS_KEY_CHECKING} ${KNOWN_HOSTS_ARG:=}  \
 -o ServerAliveInterval=30 \
 -o ServerAliveCountMax=3 \
 -t \
 -i ${SSH_KEY_FILE:=/id_rsa} \
 ${SSH_MODE:=-L} *:${LOCAL_PORT}:${REMOTE_HOST}:${REMOTE_PORT} \
 -p ${SSH_PORT:=22} \
 ${SSH_USER}@${SSH_HOSTNAME} -N
